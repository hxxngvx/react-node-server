const mongoose = require('mongoose');
const Product = require('../models/Product');

module.exports = {
    getProducts: async function (req, res, next) {
        try {
            const { page, prodPerPage } = req.query;
            const options = {
                page: parseInt(page, 10) || 1,
                limit: parseInt(prodPerPage, 4) || 4
            };

            const products = await Product.paginate({}, options)
            res.status(200).json({
                products: products.docs,
                totalPage: products.pages,
                currentPage: products.page,
                prodsPerPage: products.limit,
            })
        } catch (err) {
            next(err);
        }
    },

    createProduct: async function (req, res, next) {
        try {
            const newProduct = new Product(req.body)
            const product = await newProduct.save();
            res.status(201).json(product)
        } catch (err) {
            next(err)
        }
    },

    getProductByID: async function (req, res, next) {
        try {
            const productID = mongoose.Types.ObjectId(req.params.id)
            const product = await Product.findById(productID);
            res.status(200).json(product)
        } catch (err) {
            next(err)
        }

    },

    updateProductByID: async function (req, res, next) {
        try {
            const productID = mongoose.Types.ObjectId(req.params.id)
            const newProduct = req.body;
            const result = await Product.findByIdAndUpdate(productID, newProduct);
            res.status(200).json(result);
        } catch (err) {
            next(err)
        }
    },

    deleteProductByID: async function (req, res, next) {
        try {
            const productID = mongoose.Types.ObjectId(req.params.id)
            const result = await Product.findByIdAndRemove(productID);
            res.status(200).json(result);
        } catch (err) {
            next(err)
        }
    }
}

